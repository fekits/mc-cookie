import path from 'path';
import resolve from 'rollup-plugin-node-resolve';                        // 帮助寻找node_modules里的包
import babel from 'rollup-plugin-babel';                                 // rollup 的 babel 插件，ES6转ES5
import replace from 'rollup-plugin-replace';                             // 替换待打包文件里的一些变量，如 process在浏览器端是不存在的，需要被替换
import commonjs from 'rollup-plugin-commonjs';                           // 将非ES6语法的包转为ES6可用
import { terser } from 'rollup-plugin-terser'; // 压缩JS

const env = process.env.NODE_ENV;

// 插件名
const modelName = 'mc-cookie';

let config = {
  input: path.resolve(__dirname, `./lib/${modelName}.js`),
  output: [
    {
      file: path.resolve(__dirname, `./npm/${modelName}.umd.min.js`),
      format: 'umd',
      name: 'cookie'
    },
    {
      file: path.resolve(__dirname, `./npm/${modelName}.esm.min.js`),
      format: 'esm',
    },
    {
      file: path.resolve(__dirname, `./npm/${modelName}.cmd.min.js`),
      format: 'cjs',
    },
  ],
  plugins: [
    resolve({
      mainFields: ['module', 'jsnext:main', 'main'],
    }),
    babel({
      exclude: '**/node_modules/**'
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify(env)
    }),
    commonjs(),
    terser(),
  ],
  external: [
    // 'mc-scrollto'
  ]
};

export default config;
