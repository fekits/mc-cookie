import '../css/main.scss';
import mcCookie from '../../lib/mc-cookie';
import mcTinting from '@fekit/mc-tinting';

new mcTinting({
  theme: 'vscode',
  number: 1,
});

// 设置一个属性
let oBtn1 = document.getElementById('btn1');
oBtn1.onclick = function () {
  mcCookie.set({
    key: 'aaa',
    val: 'aaa_111$222#333@444%555*666',
    time: '+{12m7d}', // 在当前时间的基础上加上指定的年月日时分秒后过期
  });
};

// 设置多个属性
let oBtn2 = document.getElementById('btn2');
oBtn2.onclick = function () {
  mcCookie.set([
    {
      key: 'bbb',
      val: 'bbb_abc+123+中文+〜！#¥%……&*（）',
      time: '2022-10-22 10:0:0', // 设置到指定的年月日时分秒时间过期
    },
    {
      key: 'ccc',
      val: 'ccc_abc+123+中文+〜！#¥%……&*（）',
      time: 1665367200000,
    },
    {
      key: 'ddd',
      val: 'ddd_abc+123+中文+〜！#¥%……&*（）',
      time: new Date('2025-1-10 10:0:0'), // 直接传入一个时间对象
    },
  ]);
};

// 获取一个属性
let oBtn3 = document.getElementById('btn3');
oBtn3.onclick = function () {
  let aaa = mcCookie.get('aaa');
  console.log(aaa);
};

// 以数组形式获取多个属性
let oBtn4 = document.getElementById('btn4');
oBtn4.onclick = function () {
  let aList = mcCookie.get(['aaa', 'bbb', 'ccc']);
  console.log(aList);
};

// 以对象形式获取多个属性
let oBtn5 = document.getElementById('btn5');
oBtn5.onclick = function () {
  let oList = mcCookie.get({ a: 'aaa', keyName: 'bbb', abc: 'ccc' });
  console.log(oList);
};

// 获取全部属性并返回数组
let oBtn6 = document.getElementById('btn6');
oBtn6.onclick = function () {
  let all = mcCookie.all();
  console.log(all);
};

// 另外一种方便灵活的用法
let oBtn6_1 = document.getElementById('btn6_1');
oBtn6_1.onclick = function () {
  let all = mcCookie.all();

  // 我想获取aaa
  console.log(all.aaa);
  // 还需获取ccc
  console.log(all.ccc);
};

// 删除一个属性
let oBtn7 = document.getElementById('btn7');
oBtn7.onclick = function () {
  mcCookie.del('aaa');
};

// 删除多个属性
let oBtn8 = document.getElementById('btn8');
oBtn8.onclick = function () {
  mcCookie.del(['aaa', 'bbb', 'ccc']);
};

// 删除全部属性
let oBtn9 = document.getElementById('btn9');
oBtn9.onclick = function () {
  mcCookie.del('*');
};

// 判断是否有某个属性
let oBtn10 = document.getElementById('btn10');
oBtn10.onclick = function () {
  let hasAAA = mcCookie.has('aaa');
  console.log(hasAAA);
};
