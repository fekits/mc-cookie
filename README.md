# MC-COOKIE

一个操作 cookie 的常用方法。可以对 cookie 进行增删改查工作，并且可以单条或者批量操作。

#### 索引

- [演示](#演示)
- [开始](#开始)
- [传参](#传参)
- [示例](#示例)
- [版本](#版本)

#### 演示

[http://fekit.cn/plugins/mc-cookie/](http://fekit.cn/plugins/mc-cookie/)

#### 开始

NPM:

```
npm i @fekit/mc-cookie
```

CDN:

```html
<script type="text/javascript" src="https://cdn.fekit.cn/@fekit/mc-cookie/mc-cookie.umd.min.js"></script>
<script>
  cookie.set({ key: 'aaa', val: 111 });
</script>
```

#### 传参

    {
        key   : 'aaa',                           // 属性名
        val   : 'this is aaa; ',                 // 属性值
        domain: '',                              // 域名（默认不设为当前页面域名，一般不用设置）
        path  : '',                              // 域名下子路经（默认不设，一般不用设置）
        time  : '+{1y1m11d1h1n1s}'               // + 在现在开始算起的y年m月d日h时n分s秒后失效, 可以仅设置其中一个，比如1d=1天后失效
                '2022-12-12 10:0:0'              // 以字符串形式传入指定时间
                1665367200000                    // 转入时间戳
                new Date('2022-12-22 10:0:0')    // 直接传时间对象
    }

## 示例

设置一个属性:

```javascript
// 引入插件文件,后面的示例略过
import cookie from '@fekit/mc-cookie';
cookie.set({
  key: 'aaa',
  val: 'aaa_111$222#333@444%555*666',
});
```

设置多个属性:

```javascript
cookie.set([
  {
    key: 'bbb',
    val: 'bbb_abc+123+中文+〜！#¥%……&*（）',
    time: '+{1d}', // 1天后失效
  },
  {
    key: 'ccc',
    val: 'ccc_abc+123+中文+〜！#¥%……&*（）',
    time: '+{2d5h30m}', // 2天5小时30分钟后失效
  },
  {
    key: 'ddd',
    val: 'ddd_abc+123+中文+〜！#¥%……&*（）',
    time: '2022-12-12', // 在2022年12月12日0点0分0秒失败
  },
]);
```

获取一个属性:

```javascript
let aaa = cookie.get('aaa');
console.log(aaa);
```

以数组形式获取多个属性:

```javascript
let aList = cookie.get(['aaa', 'bbb', 'ccc']);
console.log(aList);
```

以对象形式获取多个属性:

```javascript
let oList = cookie.get({ a: 'aaa', keyName: 'bbb', abc: 'ccc' });
console.log(oList);
```

获取全部属性并返回数组:

```javascript
let all = cookie.all();
console.log(all);
```

另外一种方便灵活的用法:

```javascript
let all = cookie.all();

// 我想获取aaa
console.log(all.aaa);
// 还需获取ccc
console.log(all.ccc);
```

删除一个属性:

```javascript
cookie.del('aaa');
```

删除多个属性:

```javascript
cookie.del(['aaa', 'bbb', 'ccc']);
```

删除全部属性:

```javascript
cookie.del('*');
```

判断是否有某个属性:

```javascript
let hasAAA = cookie.has('aaa');
console.log(hasAAA);
```

#### 版本

```$xslt
v2.1.1 [Latest version]
1. 优化time设置失效时间的入参,添加多种入参方式
```

```$xslt
v2.0.9
1. 修复package.json配置错误
```

```$xslt
v2.0.8
1. 修改了打包工具
2. 提供了esm和umd两种方式
```

#### 反馈

```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
QQ: 860065202
EMAIL: xiaojunbo@126.com
```
