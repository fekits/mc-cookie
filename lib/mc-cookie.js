const pre = (time) => {
  if (time) {
    const now = new Date();
    const fun = { y: 'FullYear', m: 'Month', d: 'Date', h: 'Hours', n: 'Minutes', s: 'Seconds' };
    if (typeof time === 'string') {
      time.replace(/\+\{(\w*)\}/g, (o, s) => {
        s.replace(/(\d*)([ymdhns])/g, (o, n, t) => {
          now['set' + fun[t]](now['get' + fun[t]]() + Number(n));
        });
      });
      time = new Date(now);
    } else if (typeof time === 'number') {
      time = new Date(time);
    }
  }
  return time;
};

const cookie = {
  set: function (param) {
    function _set(info) {
      let { key = '', val = '', domain = '', time = '', path = '' } = info;
      time = pre(time);
      // console.log(time);
      document.cookie = encodeURIComponent(key) + '=' + encodeURIComponent(val) + ';domain=' + domain + ';expires=' + time + ';path=' + path;
    }

    if (Object.prototype.toString.call(param) === '[object Array]') {
      param.map((task) => {
        if (task.key) {
          _set(task);
        } else {
          console.error('key cannot be empty', task);
        }
      });
    } else if (Object.prototype.toString.call(param) === '[object Object]') {
      if (param.key) {
        _set(param);
      } else {
        console.error('key cannot be empty', param);
      }
    } else {
      console.error('Param error ', param);
    }
  },
  get: function (param) {
    if (Object.prototype.toString.call(param) === '[object String]') {
      return cookie.all()[param];
    } else if (Object.prototype.toString.call(param) === '[object Array]') {
      return param.map((item) => {
        return cookie.all()[item];
      });
    } else if (Object.prototype.toString.call(param) === '[object Object]') {
      let db = {};
      for (let item in param) {
        if (param.hasOwnProperty(item)) {
          db[item] = cookie.all()[param[item]];
        }
      }
      return db;
    } else {
      console.error('param error');
    }
  },
  has: function (sKey) {
    return cookie.all().hasOwnProperty(sKey);
  },
  del: function (param) {
    if (Object.prototype.toString.call(param) === '[object String]') {
      if (param === '*') {
        param = [];
        let allCookie = cookie.all();
        for (let item in allCookie) {
          if (allCookie.hasOwnProperty(item)) {
            param.push(item);
          }
        }
      } else {
        param = [param];
      }
    }
    param.map((key) => {
      document.cookie = encodeURIComponent(key) + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT' + (key.domain ? '; domain=' + key.domain : '') + (key.path ? '; path=' + key.path : '');
    });
  },
  all: function () {
    let db = {};
    if (document.cookie) {
      document.cookie.split(/;\s+/).map(function (item) {
        let key = '';
        let val = '';

        try {
          key = decodeURIComponent(item.split('=')[0]);
        } catch (e) {
          key = item.split('=')[0];
        }

        try {
          val = decodeURIComponent(item.split('=')[1]);
        } catch (e) {
          val = item.split('=')[1];
        }

        db[key] = val;
      });
    }
    return db;
  },
};
export default cookie;
